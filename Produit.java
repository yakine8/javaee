public class Produit implements Serializable {
		private Long id ;
		private String libelle ;
		private Double prixUnitaire ;
		private Date datePeremption ;

		public Produit (){
			this.id = 0 ;
			this.libelle = "";
			this.prixUnitaire = 0 ;
			this.datePeremption = NullPointerException;
		}

		public Produit (Long id , String libelle , Double prixUnitaire , Date datePeremption){
			this.id = id ;
			this.libelle = libelle ;
			this.prixUnitaire = prixUnitaire;
			this.datePeremption = datePeremption ;
		}

		public Long getId(){
			return this.id;
		}

		public String getLibelle(){
			return this.libelle ;
		}

		public double getPrixUnitaire(){
			return this.prixUnitaire ;
		}

		public Date getDatePeremption(){
			return this.datePeremption;
		}


		public void setId(Long id ){
		 this.id = id ;
		}

		public void setLibelle(String libelle){
		 this.libelle = libelle;
		}

		public void setPrixUnitaire( double prixUnitaire ){
			this.prixUnitaire = prixUnitaire;
		}

		public void setDatePeremption( Date datePeremption){
		 this.datePeremption = datePeremption;
		}






		public boolean estPerime() {

		}

		public boolean estPerime(Date referenceDate){

		}

}